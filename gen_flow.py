import cv2
import numpy as np
import os
import argparse
from natsort import natsorted


def crop(image, center, radius, size=512):
    scale = 1.3
    radius_crop = (radius * scale).astype(np.int32)
    center_crop = (center).astype(np.int32)

    rect = (max(0, (center_crop - radius_crop)[0]),
            max(0, (center_crop - radius_crop)[1]),
            min(512, (center_crop + radius_crop)[0]),
            min(512, (center_crop + radius_crop)[1]))

    image = image[rect[1]:rect[3], rect[0]:rect[2], :]

    if image.shape[0] < image.shape[1]:
        top = abs(image.shape[0] - image.shape[1]) // 2
        bottom = abs(image.shape[0] - image.shape[1]) - top
        image = cv2.copyMakeBorder(image,
                                   top,
                                   bottom,
                                   0,
                                   0,
                                   cv2.BORDER_CONSTANT,
                                   value=(0, 0, 0))
    elif image.shape[0] > image.shape[1]:
        left = abs(image.shape[0] - image.shape[1]) // 2
        right = abs(image.shape[0] - image.shape[1]) - left
        image = cv2.copyMakeBorder(image,
                                   0,
                                   0,
                                   left,
                                   right,
                                   cv2.BORDER_CONSTANT,
                                   value=(0, 0, 0))
    return image


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_path",
                        type=str,
                        default="val",
                        help="Path to Input videos dataset")
    parser.add_argument("--output_path",
                        type=str,
                        default="rgb_frames",
                        help="Path to Output of RGB frames")
    parser.add_argument("--skeleton_path",
                        type=str,
                        default="val_npy/npy3",
                        help="Path to Input extracted Skeleton Keypoints data")
    parser.add_argument("--skip",
                        type=bool,
                        default=False,
                        help="Skip already existing extracted frames")
    parser.add_argument(
        "--video_type",
        type=str,
        default='_color',  # valid values: 'color.mp4', 'depth.mp4'
        help="Choose the particular type of videos ")
    cmd_args = parser.parse_args()
    selected_joints = np.concatenate(([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], [
        91, 95, 96, 99, 100, 103, 104, 107, 108, 111
    ], [112, 116, 117, 120, 121, 124, 125, 128, 129, 132]),
                                     axis=0)

    # of = optical flow
    input_path = cmd_args.input_path  # 'output_file_train', 'output_file_test'
    skeleton_path = cmd_args.skeleton_path  # 'train_npy/npy3', 'test_npy/npy3'
    output_path = cmd_args.output_path  # 'train_flow_depth', 'test_flow_depth', 'train_flow_color', 'val_flow_color', 'test_flow_color'
    video_type = cmd_args.video_type  # 'color', 'depth'
    video_type_extension_lenth = len(video_type)

    for video_path in natsorted(os.listdir(input_path)):
        input_video_dir = os.path.join(input_path, video_path)
        if os.path.isdir(input_video_dir) and 'color' in video_path:
            video_dir = video_path[:-video_type_extension_lenth]
            output_video_dir = os.path.join(output_path, video_dir)
            if cmd_args.skip and os.path.isdir(output_video_dir) and len(
                    os.listdir(output_video_dir)) > 0:
                print(video_path + ' - done.')
                continue
            print(output_video_dir)
            frames = []
            current_read_dir = os.path.join(input_path, video_path)
            for name in natsorted(
                    os.listdir(os.path.join(input_path, video_path))):
                if 'flow_x' in name:
                    frames.append(name)
            print(video_dir)
            skeleton = np.load(
                os.path.join(skeleton_path,
                             video_dir + '_color.mp4.npy')).astype(np.float32)
            skeleton = skeleton[:, selected_joints, :2]
            skeleton[:, :, 0] = 512 - skeleton[:, :, 0]
            xy_max = skeleton.max(axis=1, keepdims=False).max(axis=0,
                                                              keepdims=False)
            xy_min = skeleton.min(axis=1, keepdims=False).min(axis=0,
                                                              keepdims=False)
            assert xy_max.shape == (2, )
            xy_center = (xy_max + xy_min) / 2 - 20
            xy_radius = (xy_max - xy_center).max(axis=0)
            if not os.path.exists(output_video_dir):
                os.mkdir(output_video_dir)

            for f in frames:
                img_x = cv2.imread(os.path.join(current_read_dir, f),
                                   cv2.IMREAD_GRAYSCALE)
                img_y = cv2.imread(
                    os.path.join(current_read_dir, f.replace('x', 'y')),
                    cv2.IMREAD_GRAYSCALE)
                # print(
                #     os.path.join(current_read_dir,
                #                  '{:04d}.jpg'.format(int(f[7:-4]))))
                img = np.zeros((img_y.shape[0], img_y.shape[1], 3),
                               dtype=np.uint8)
                img[:, :, 2] = img_x
                img[:, :, 1] = img_y
                img[:, :, 0] = img_x

                img = cv2.resize(img, (512, 512))
                img = crop(img, xy_center, xy_radius)
                img = cv2.resize(img, (256, 256))
                cv2.imwrite(
                    os.path.join(output_video_dir,
                                 '{:04d}.jpg'.format(int(f[7:-4]))), img)


if __name__ == '__main__':
    main()
